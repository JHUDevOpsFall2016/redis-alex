FROM  centos:7
MAINTAINER  Alex Goodman

# Install build and run dependencies for Redis
RUN yum install -y gcc tcl wget make autoconf kernel-devel gcc-c++

# Put Redis source code into the container directory tree.
# Note: the destination path should include the FULL dir path (including the
# directory name of the source)
COPY redis/ /opt/redis
WORKDIR  /opt/redis

# Build and install Redis
#RUN make MALLOC=libc && make install
RUN make && make install

# Copy the redis.conf into place
RUN mkdir -p /etc/redis
RUN cp -f *.conf /etc/redis

# Comment out unneeded options
RUN sed -i 's/^\(bind .*\)$/# \1/' /etc/redis/redis.conf && \
    sed -i 's/^\(daemonize .*\)$/# \1/' /etc/redis/redis.conf && \
    sed -i 's/^\(logfile .*\)$/# \1/' /etc/redis/redis.conf

# Define the data volume in the redis.conf and for docker
RUN sed -i 's/^\(dir .*\)$/# \1\ndir \/data/' /etc/redis/redis.conf
VOLUME ["/data"]

# Remove the build directory and build dependencies
WORKDIR  /data
RUN rm -rf /tmp/redis
RUN yum remove -y gcc wget make autoconf kernel-devel gcc-c++ && yum clean all

# Define how to start Redis and expose the TCP port Redis is serving on to the
# outside world
CMD ["redis-server", "/etc/redis/redis.conf"]
EXPOSE 6379
