all: redis

redis:
	@cd redis && make

vagrant:
	@vagrant up

docker:
	@sudo docker build -t redis .

.PHONY: all redis vagrant docker
