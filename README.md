# Redis 

Checkout the README.md in ./redis

## Dependencies
Targets installation is on Centos 7.

* Requires gcc and tcl
* Ansible

## Building Redis

It is as simple as:
```
cd redis
make
```
After building Redis, it is a good idea to test it using:
```
make test
```

... or redis can be installed in a vagrant VM:
```
vagrant up
vagrant ssh
/vagrant/redis/src/redis-server
```

... or redis can be installed in a Docker container:
```
sudo docker build -t redis .
sudo docker run -p 6379:6379 redis
```
